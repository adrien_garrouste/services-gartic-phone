const GameStatus = require('../enums/GameStatus');

module.exports = (req, res, next) => {
    req.checkStart = (gamesManager) => {
        const game = gamesManager.getGame(req.params.id);
        if (game && game.getCreator().getUsername() === req.body.username) {
            game.setStatus(GameStatus.WRITING_SENTENCES);
            res.status(200).send(game);
        } else {
            res.status(400).send({"Error": "Seul le créateur peut lancer la partie."});
        }
    }
    next();
}