module.exports = (req, res, next) => {
    req.assignSentence = (gamesManager) => {
        const { sentence, player } = req.body;
        const game = gamesManager.getGame(req.params.id);

        if (!sentence || !player || !game) {
            return res.status(200).send({"Error": "Une erreur est survenue."});
        }

        const currentPlayer = game.getPlayer(player.username);

        if (!currentPlayer) {
            return res.status(404).send({"Error": "Joueur introuvable"});
        }

        if (sentence && sentence !== '') {
            currentPlayer.setSentence(sentence);
        }

        res.status(200).send({ sentence });
    }
    next();
}