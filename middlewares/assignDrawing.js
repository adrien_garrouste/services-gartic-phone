module.exports = (req, res, next) => {
    req.assignDrawing = (gamesManager) => {
        const game = gamesManager.getGame(req.params.id);
        const localPlayer = game.getPlayer(req.body.player.username);
        localPlayer.setDrawing(req.body.dataDraw);
    }
    next();
}