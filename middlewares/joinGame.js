const Player = require('../models/Player');

module.exports = (req, res, next) => {
    req.joinGame = (gamesManager) => {

        const game = gamesManager.getGame(req.params.id);

        if (!game) {
            return res.status(404).send({"Error": "Partie introuvable"});
        }

        const userExists = game.checkIfNameIsUnique(req.body.username);

        if (userExists) {
            return res.status(404).send({"Error": "Un joueur porte déjà ce nom."});
        }

        const player = new Player(req.body.username,
                                req.body.picture);
        gamesManager.addPlayer(req.params.id, player);
        res.status(200).send(game);
    }
    next();
}