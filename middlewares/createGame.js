const Game = require('../models/Game');
const Player = require('../models/Player');

module.exports = (req, res, next) => {
    req.createGame = (gamesManager) => {
        const player = new Player(req.body.username,
                                req.body.picture);
        const game = new Game(gamesManager.generateGameId(), player);
        gamesManager.add(game);
        res.status(200).json(game);
    }
    next();
}