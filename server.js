const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
    cors: {
        origin: "http://localhost:4200",
    }
});

app.use(express.json());

/* Personnal middlewares */
app.use(require('./middlewares/headers'));
app.use(require('./middlewares/createGame'));
app.use(require('./middlewares/joinGame'));
app.use(require('./middlewares/checkStart'));
app.use(require('./middlewares/assignSentence'));
app.use(require('./middlewares/assignDrawing'));

const GamesManager = require('./models/GamesManager');
const GameStatus = require('./enums/GameStatus');

const TIME = 20000;
const gamesManager = new GamesManager();

app.get('*', (req, res) => {
    res.status(404).send({"Error": "Not found"});
});

app.post('/games', (req, res, next) => {
    req.createGame(gamesManager);
})

app.post('/games/:id', (req, res, next) => {
    req.joinGame(gamesManager);
});

app.post('/games/:id/start', (req, res, next) => {
    req.checkStart(gamesManager);
});

app.post('/games/:id/sentence', (req, res, next) => {
    req.assignSentence(gamesManager);
});

app.post('/games/:id/drawing', (req, res, next) => {
    req.assignDrawing(gamesManager);
});

server.listen(8100);

io.on('connection', client => {

    client.on('leaveGame', (data) => {
        console.log(data)
    });

    client.on('create', (gameId) => {
        client.join(gameId);
    });

    client.on('join', (gameId) => {
        client.join(gameId);
        io.to(gameId).emit('game', gamesManager.getGame(gameId));
    });

    client.on('start', (gameId) => {
        io.to(gameId).emit('game', gamesManager.getGame(gameId));
    });

    client.on('writeMoment', (data) => {
        const game =  gamesManager.getGame(data.gameId);
        const player = game.getPlayer(data.player.username);
        setTimeout(() => {
            game.checkIfAllPlayersHaveSentence();
            game.setStatus(GameStatus.DRAWING);
            io.to(data.gameId).emit('game', game);
        }, TIME);
    });

    client.on('drawMoment', (gameId) => {
        const game =  gamesManager.getGame(gameId);
        setTimeout(() => {
            game.mixImageBetweenPlayers();
            game.setStatus(GameStatus.GUESSING);
            io.to(gameId).emit('game', game);
        }, TIME + TIME / 2);
    });
});