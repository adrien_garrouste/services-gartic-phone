module.exports = {
    WAITING_PLAYERS: 1,
    WRITING_SENTENCES: 2,
    DRAWING: 3,
    GUESSING: 4,
    END: 5,
    WAITING_SERVER: 6
};