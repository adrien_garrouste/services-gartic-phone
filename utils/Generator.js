class Generator {

    static SENTENCES = [
        "Vin Diesel qui décrit ses plantes préférées",   
        "Expliquer Roblox à un boomer",
        "Une imprimante se fait arracher une dent",  
        "Une tomate en retard pour un meeting important",   
        "Aimer le fromage mais être intolérant aux lactoses",    
        "Un cochon astronaute",   
        "Quelqu'un qui hésite à aller à une plage nudiste",   
        "Yoda apprend à faire de la raquette",   
        "Un chien qui a des crampes",   
        "Pain au chocolat VS chocolatine",   
        "Pikachu se fait lui-même électrocuter",   
        "Hulk découvre les joies de la paternité",   
        "Grand-maman devient DJ dans un rave",   
        "GTA mais avec des légumes",   
        "Des pantalons un peu snob",   
        "Un chien médecin",   
        "Yoshi gagne un marathon", 
        "Le premier jour de printemps",   
        "Lord of the Rings à la plage",   
        "Deux loutres en chicane",  
        "Spider-Man fait une danse TikTok",  
        "Bébé Yoda prend le thé",   
        "Un oiseau gangster",   
        "Fall Guys mais plus personne y joue",   
        "Un Pokémon passe la tondeuse",   
        "Un appel Zoom avec un chat",   
        "Un mime en prison",   
        "Une soirée Karaoké qui vire mal",   
        "Une patate au four toute garnie",   
        "[Ami] qui pleure en regardant au loin",   
        "Un chirurgien échappe des ciseaux dans un patient",  
        "Mickey Mouse est un anti-masque",  
        "Les funérailles d'une pointe de pizza",   
        "Une comédie musicale Star Wars",   
        "Les gros doigts de Gildor Roy",   
        "Un chat qui se transforme graduellement en humain",  
        "Un tracteur amoureux",   
        "Les tortues prennent le contrôle de la planète",   
        "Le pain très spécial d'un boulanger",   
        "Le nouveau produit miracle vendu à la télé",   
        "Tom Cruise se fâche sur un plateau de tournage",   
        "Le nouvel emploi de Donald Trump",  
        "Invasion de zombies à Paris",  
        "Un lapin joue au bowling",
        "Un chien à la plage",
        "Une mamie qui fait du tennis",
        "Un papi sans dent qui sourit",
        "Un pirate qui fait du pole dance",
        "Un chinois au McDo",
        "Un bodybuilder en tutu"
    ];

    static MAX_LENGTH = 8;

    static ALPHANUM = "ABCEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    static generateId() {      
        let id = "";
        let index;
        for (let i = 0; i < Generator.MAX_LENGTH; i++) {
            index = Math.floor(Math.random() * Generator.ALPHANUM.length - 1);
            id += Generator.ALPHANUM[index >= 0 ? index : 0];
        }
        return id;
    }

    static generateRandomSentence() {
        let index = Math.floor(Math.random() * Generator.SENTENCES.length - 1);
        return Generator.SENTENCES[index >= 0 ? index : 0];
    }
}

module.exports = Generator;