const GameStatus = require('../enums/GameStatus');
const Generator = require('../utils/Generator');

class Game {

    constructor(id, creator) {
        this.id = id;
        this.creator = creator;
        this.players = [];
        this.players.push(creator);
        this.status = GameStatus.WAITING_PLAYERS;
    }

    addPlayer(player) {
        this.players.push(player);
    }

    getPlayer(playerName) {
        return this.players.find(player => player.username === playerName);
    }

    checkIfNameIsUnique(nameToCheck) {
        return this.players.find(player => player.username === nameToCheck);
    }

    getCreator() {
        return this.creator;
    }

    setStatus(newStatus) {
        this.status = newStatus;
    }

    checkIfAllPlayersHaveSentence() {
        for (let player of this.players) {
            if (player.getSentence() === '') {
                player.setSentence(Generator.generateRandomSentence());
            }
        }
    }

    mixImageBetweenPlayers() {
        let copy = JSON.parse(JSON.stringify(this.players));
        for (let i = 0; i < this.players.length; i++) {
            this.players[i].toGuess = copy[(i + 1) % copy.length].drawing;
        }
    }

    isReadyToGuess() {
        return this.players.filter(player => player.toGuess != '').length === this.players.length;
    }
}

module.exports = Game;