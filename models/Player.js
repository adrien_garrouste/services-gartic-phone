class Player {

    constructor(username, picture) {
        this.username = username;
        this.picture = picture;
        this.sentence = '';
        this.drawing = '';
        this.toGuess = '';
    }

    getUsername() {
        return this.username;   
    }

    getSentence() {
        return this.sentence;
    }

    setSentence(newSentence) {
        this.sentence = newSentence;
    }

    setDrawing(newDrawing) {
        this.drawing = newDrawing;
    }
}

module.exports = Player;