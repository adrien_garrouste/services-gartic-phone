const Generator = require('../utils/Generator');

class GamesManager {

    constructor() {
        this.games = [];
    }

    add(game) {
        this.games.push(game);
    }

    addPlayer(gameId, player) {
        let game = this.getGame(gameId);

        if (game) {
            game.addPlayer(player);
        }
    }
    
    getGame(id) {
        return this.games.find(game => game.id == id);
    }

    generateGameId() {
        return this.games.length + Generator.generateId();
    }
}

module.exports = GamesManager;